const config = require('config')
const register = require('./register')
const series = require('run-series')
const server = require('./server')

const plugins = [{
  package: require('good'),
  options: {
    ops: {
      interval: 1000
    },
    reporters: {
      console: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{
          error: '*',
          log: '*',
          request: '*',
          response: '*'
        }]
      }, {
        module: 'good-console'
      }, 'stdout']
    }
  }
}, require('inert'), {
  package: require('huyang'),
  options: config.get('webpack')
}, {
  package: require('./api'),
  registerOptions: {
    routes: {
      prefix: '/api'
    }
  }
}, require('./app')]

register(server, plugins, (err) => {
  if (err) {
    throw err
  }

  series([
    start,
    running
  ], (err) => {
    if (err) {
      throw err
    }
  })
})

function start (cb) {
  return server.start(cb)
}

function running (cb) {
  server.log(['start'], 'running')
  return cb()
}
