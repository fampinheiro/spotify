const series = require('run-series')

module.exports = (server, plugins, cb) => {
  const tasks = plugins.map((plugin) => {
    return (cb) => {
      const {
        options = {},
        package = plugin,
        registerOptions = {}
      } = plugin

      return server.register({
        options,
        register: package
      }, registerOptions, cb)
    }
  })

  return series(tasks, cb);
}
