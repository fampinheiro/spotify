const React = require('react')

module.exports = (props) => {
  const {
    loading = false
  } = props

  if (!loading) {
    return null
  }

  return (
    <div className='c-loading'></div>
  )
}
