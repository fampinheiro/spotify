const get = require('lodash.get')
const React = require('react')

module.exports = (props) => {
  const handleSubmit = (e) => {
    e.preventDefault()
    const method = props.account ? 'onLogout' : 'onLogin'
    if (props[method]) {
      props[method]()
    }
  }

  const action = props.account ? '/login' : '/logout'
  const value = props.account ? 'Bye' : 'Login'

  return (
    <form action={action} className='container v-grid v-grid--between v-grid--center-items'
      method='get' onSubmit={handleSubmit}>
      <h1 className='v-text--white'>{`Hello, ${get(props, 'account.displayName', 'Stranger')}`}</h1>
      <input type='submit' className='v-button v-button--primary' value={value} />
    </form>
  )
}
