const array = require('stream-to-array')
const csv = require('csv-parser')
const Dropzone = require('react-dropzone')
const noop = require('lodash.noop')
const pump = require('pump')
const React = require('react')
const reader = require('filereader-stream')
const RR = require('react-router')

const {
  Link
} = RR

module.exports = (props) => {
  const {
    onError = noop,
    onSearch = noop
  } = props

  const dropzoneProps = {
    activeStyle: {
      opacity: 0.5,
    },
    accept: 'text/csv',
    multiple: false,
    onDrop: onDrop,
    style: {
      flex: 1
    }
  }

  return (
      <Dropzone {...dropzoneProps}>
        {props.children}
      </Dropzone>
  )

  function onDrop (files) {
    if (!files.length) {
      return
    }

    array(reader(files[0]).pipe(csv()), (err, rows) => {
      if (err) {
        return onError(err)
      }

      if (rows) {
        return onSearch({
          columns:  Object.keys(rows[0]),
          rows
        })
      }

      return onError('No rows on this file')
    })
  }
}
