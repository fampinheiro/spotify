require('./styles/main.styl')
const App = require('./pages/app')
const React = require('react')
const render = require('react-dom').render
const router = require('react-router')
const RR = require('react-redux')
const store = require('./store')

const {
  browserHistory,
  Router
} = router

const {
  Provider
} = RR

const app = (
  <Provider store={store}>
    <Router history={browserHistory} routes={require('./routes')} />
  </Provider>
)

render(app, document.querySelector('#mount'))
