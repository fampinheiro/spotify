const get = require('lodash.get')
const noop = require('lodash.noop')
const React = require('react')
const uuid = require('node-uuid')

module.exports = React.createClass({
  componentWillReceiveProps: function (props) {
    this.setState(getInitialState(props))
  },
  getInitialState: function () {
    return getInitialState(this.props)
  },
  render: function () {
    const {
      cover = 'https://placehold.it/640x640',
      href,
      message,
      readOnly = true,
      onClose
    } = this.props

    const {
      title,
      artist
    } = this.state

    const style = {
      backgroundImage: `url("${cover}")`
    }

    const handleClose = () => {
      if (onClose) {
        onClose()
      }
    }

    const handleChange = (prop, event) => {
      this.setState(Object.assign({}, this.state, {
        [prop]: event.target.value
      }))
    }

    const handleSubmit = (event) => {
      event.preventDefault()
      if (this.props.onSubmit) {
        this.props.onSubmit({
          title: this.state.title,
          artist: this.state.artist
        })
      }
    }

    const form = `f${uuid.v4()}`
    const closeComponent = onClose ? (
      <div className='c-close' onClick={handleClose}>
        <i className='fa fa-times' aria-hidden='true' />
      </div>
    ) : null

    const titleComponent = (
      <div className='v-grid'>
        <Title form={form} href={get(this.props, 'href')} value={title}
          onChange={!readOnly ? handleChange.bind(this, 'title') : null} />
        {closeComponent}
      </div>
    )

    const subtitleComponent = (
      <div className='v-grid v-grid__item'>
        <Subtitle form={form} href={get(this.props, 'artist.href')} value={artist}
          onChange={!readOnly ? handleChange.bind(this, 'artist') : null} />
      </div>
    )

    const formComponent = readOnly ? null : (
      <form action='/playlist/0' className='v-0 v-grid__item v-grid v-grid--end'
        id={form} method='post' onSubmit={handleSubmit}>
        <input className='c-track__button v-margin-0 v-grid__item--end v-button v-button--submit'
          type='submit' value='Add' />
      </form>
    )

    const messageComponent = message ? (
      <div className='c-track__message v-message v-message--error v-grid v-grid--end-items'>
        <p className='v-message__text v-text v-text--center v-grid__item'>{message}</p>
      </div>
    ) : null

    const imageComponent = href ? (
      <a href={get(this.props, 'album.href')}>
        <div className='c-track__image' style={style} />
      </a>
    ) : (
      <div className='c-track__image' style={style} />
    )

    return (
      <div className='c-track v-grid'>
        <div className='v-grid__item v-grid__item--33'>
          {imageComponent}
        </div>
        <div className='c-track__content v-grid__item v-grid v-grid--column'>
          {titleComponent}
          {subtitleComponent}
          {messageComponent}
          {formComponent}
        </div>
      </div>
    )
  }
})

function getInitialState (props) {
  const {
    title,
    artist
  } = props

  return {
    title: get(title, 'name', 'Title'),
    artist: get(artist, 'name', 'Artist')
  }
}

function Title (props) {
  const {
    form,
    href,
    onChange,
    value
  } = props

  const componentProps = {
    form,
    onChange,
    value
  }

  if (onChange) {
    return (
      <input {...componentProps} className='c-track__title' type='text' />
    )
  }

  const component = href ? (
    <a href={href}>{value}</a>
  ) : value

  return (
    <h1 className='c-track__title'>{component}</h1>
  )
}

function Subtitle (props) {
  const {
    form,
    href,
    onChange,
    value
  } = props

  const componentProps = {
    form,
    onChange,
    value
  }

  if (onChange) {
    return (
      <input {...componentProps} className='c-track__artist' type='text' />
    )
  }

  const component = href ? (
    <a href={href}>{value}</a>
  ) : value

  return (
    <h2 className='c-track__artist'>
      {component}
    </h2>
  )
}
