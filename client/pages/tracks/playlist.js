const isEmpty = require('lodash.isempty')
const map = require('lodash.map')
const noop = require('lodash.noop')
const React = require('react')
const Tracks = require('./tracks')
const uuid = require('node-uuid')

module.exports = React.createClass({
  componentWillReceiveProps: function (props) {
    this.setState(Object.assign({}, this.state, getInitialState(props)))
  },
  getInitialState: function () {
    return getInitialState(this.props)
  },
  render: function () {
    const {
      readOnly,
      tracks,
      onSubmit = noop,
      onClose
    } = this.props

    const {
      title
    } = this.state

    const handleChange = (event) => {
      this.setState(Object.assign({}, this.state, {
        title: event.target.value
      }))
    }

    const handleSubmit = (event) => {
      event.preventDefault()

      onSubmit(this.state.title, map(this.props.tracks, (track) => {
        return track.id
      }))
    }

    const form = `f${uuid.v4()}`
    const titleComponent = readOnly ? (
      <h1 className='c-playlist__title'>{title}</h1>
    ) : (
      <input className='c-playlist__title v-grid__item' form={form} type='text'
        onChange={handleChange} value={title} />
    )

    const formComponent = readOnly ? null : (
      <form action='/playlist/0' className='v-0 v-grid__item v-grid v-grid--end'
        id={form} method='post' onSubmit={handleSubmit}>
        <input className='v-grid__item--end v-button v-button--submit'
          type='submit' value='Create Playlist' />
      </form>
    )

    const component = !isEmpty(tracks) ? (
      <div className='v-grid v-grid--center-items'>
        {titleComponent}
        {formComponent}
      </div>
    ) : null

    return (
      <div className='c-playlist'>
        {component}
        <Tracks onClose={onClose} tracks={tracks} />
      </div>
    )
  }
})

function getInitialState (props) {
  const {
    title = 'My Playlist'
  } = props

  return {
    title
  }
}
