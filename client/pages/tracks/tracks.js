const isEmpty = require('lodash.isempty')
const map = require('lodash.map')
const React = require('react')
const Track = require('./track')

module.exports = (props) => {
  const {
    onClose = noop,
    tracks
  } = props

  if (isEmpty(tracks)) {
    return (
      <div className='c-tracks'>
        <p className='v-text v-text--center'>Your tracks will appear here</p>
      </div>
    )
  }

  return (
    <div className='c-tracks'>
      {map(props.tracks, (track, i) => {
        return (
          <Track key={i} onClose={onClose.bind(this, track.id)} readOnly {...track} />
        )
      })}
    </div>
  )
}
