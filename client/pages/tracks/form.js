const map = require('lodash.map')
const React = require('react')
const noop = require('lodash.noop')
const Track = require('./track')
const uuid = require('node-uuid')

module.exports = React.createClass({
  getInitialState: function () {
    const {
      columns
    } = this.props

    return {
      artist: columns[1],
      title: columns[0]
    }
  },
  render: function () {
    const {
      columns,
      onClear = noop,
      onSubmit = noop
    } = this.props


    const state = this.state
    const form = `f${uuid.v4()}`
    const style = {
      backgroundImage: `url("https://placehold.it/640x640")`
    }

    const handleClear = (e) => {
      e.preventDefault()
      return onClear()
    }

    const handleChange = (prop, e) => {
      this.setState(Object.assign({}, this.state, {
        [prop]: e.target.value
      }))
    }

    const handleSubmit = (e) => {
      e.preventDefault()
      return onSubmit(this.state)
    }

    return (
      <div>
        <div className='c-track v-grid'>
          <div className='v-grid__item v-grid__item--33'>
            <div className='c-track__image' style={style} />
          </div>
          <div className='v-grid__item v-grid v-grid--column'>
            <div className='v-grid v-grid--center-items'>
              <h1 className='c-track__title'>Title</h1>
              {select('title')}
            </div>
            <div className='v-grid v-grid--center-items'>
              <h2 className='c-track__artist'>Artist</h2>
              {select('artist')}
            </div>
            <form action='/playlist/0' className='v-0 v-grid__item v-grid v-grid--end'
              id={form} method='post' onSubmit={handleSubmit}>
              <input className='c-track__button v-margin v-grid__item--end v-button'
                onClick={onClear} type='button' value='Clear' />
              <input className='c-track__button v-margin v-grid__item--end v-button v-button--submit'
                type='submit' value='Add' />
            </form>
          </div>
        </div>
      </div>
    )

    function select (prop) {
      return (
        <select className='v-text v-text--center'
          onChange={handleChange.bind(this, prop)}
          value={state[prop]}>
          {map(columns, (option, i) => {
            return (
              <option key={i} value={option}>
                {option}
              </option>
            )
          })}
        </select>
      )
    }
  }
})
