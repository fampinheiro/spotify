const array = require('force-array')
const get = require('lodash.get')
const map = require('lodash.map')
const noop = require('lodash.noop')
const React = require('react')
const router = require('react-router')
const RR = require('react-redux')
const store = require('../../store')
const Track = require('./track')
const Playlist = require('./playlist')
const Upload = require('../../views/upload')
const Form = require('./form')

const {
  Link,
  withRouter
} = router

const {
  connect
} = RR

const {
  actions
} = store

const view = React.createClass({
    getInitialState: function () {
      return {
        name: 'My Playlist',
        valid: true
      }
    },
    render: function () {
      const {
        tracks,
        onClear = noop,
        onClose,
        onError = noop,
        onFile = noop,
        onSubmit = noop,
        onSearch = noop,
        onUpload = noop,
        onPlaylist = noop
      } = this.props

      const {
        name,
        valid
      } = this.state

      const fileProps = {
        columns: get(this.props, 'raw.tracks.columns', null),
        onSubmit: onUpload,
        onClear: onClear
      }

      const formComponent = fileProps.columns ? (
        <Form {...fileProps} />
      ) : (
        <Track readOnly={false} onSubmit={onSearch}/>
      )

      const uploadProps = {
        className: 'v-grid__item v-message__text v-text v-text--center',
        onError,
        onSearch: onFile
      }

      return (
        <div>
          <div className='v-grid v-message'>
            <Upload {...uploadProps}>
              <p className='v-grid__item v-message__text v-text v-text--center'>
                Drop your comma separated values here
              </p>
            </Upload>
          </div>
          <div className='container'>
            {formComponent}
            <Playlist readOnly={false} tracks={tracks} onClose={onClose}
              onSubmit={onPlaylist} />
          </div>
        </div>
      )
    }
  })

module.exports = connect(
  mapStateToProps,
  mapDispatchToProps
)(view)

function mapStateToProps (state) {
  return state
}

function mapDispatchToProps (dispatch) {
  return {
    onClear: () => {
      return dispatch(actions.tracks.raw.clear())
    },
    onError: (err) => {
      console.error(err)
      return dispatch(actions.errors.new())
    },
    onFile: (file) => {
      return dispatch(actions.tracks.raw(file))
    },
    onPlaylist: (name, tracks) => {
      return dispatch(actions.playlists.new({
        name,
        tracks
      }))
    },
    onUpload: (columns) => {
      return dispatch(actions.tracks.raw.search(columns))
    },
    onSearch: (track) => {
      return dispatch(actions.tracks.search(array(track)))
    },
    onClose: (id) => {
      return dispatch(actions.tracks.close(id))
    }
  }
}
