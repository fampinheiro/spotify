const Account = require('../views/account')
const Loading = require('../views/loading')
const React = require('react')
const router = require('react-router')
const RR = require('react-redux')
const store = require('../store')
const Tracks = require('./tracks')

const {
  actions
} = store

const {
  withRouter
} = router

const {
  connect
} = RR

const view = (props) => {
  const {
    loading
  } = props

  const tracks = (
    <Tracks />
  )

  return (
    <div>
      <div className='c-account'>
        <Account {...props} />
      </div>
      <div>
        {props.children || tracks}
      </div>
      <Loading loading={loading} />
    </div>
  )
}

module.exports = withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
  )(view)
)

function mapStateToProps({
  account,
  loading
}) {
  return {
    account,
    loading
  }
}

function mapDispatchToProps (dispatch) {
  return {
    onLogout: () => {
      return dispatch(actions.account.logout())
    }
  }
}

function mergeProps (stateProps, dispatchProps, ownProps) {
  return Object.assign({}, stateProps, dispatchProps, ownProps, {
    onLogin: () => {
      return actions.account.login()
    }
  })
}
