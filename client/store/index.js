const get = require('lodash.get')
const redux = require('redux')
const pkg = require('../../package.json')
const storage = require('local-storage')

const KEY = 'REDUX_STORE_STATE'
const {
  applyMiddleware,
  createStore
} = redux


const store = createStore(
  require('./reducers'),
  Object.assign({}, load(), get(global, KEY, {})),
  applyMiddleware(...require('./middlewares').concat(save))
)

store.actions = require('./actions')
module.exports = store

function save (store) {
  return (dispatch) => (action) => {
    const state = dispatch(action)
    storage.set(KEY, {
      state: store.getState(),
      version: pkg.version
    })
    return state
  }
}

function load () {
  try {
    const state = storage.get(KEY)
    return state.version === pkg.version ? state.state : {}
  } catch (e) {
    return {}
  }
}
