const api = require('../api')

module.exports = [
  require('redux-promise-middleware').default(),
  require('redux-thunk').default.withExtraArgument(api),
  require('redux-logger')()
]
