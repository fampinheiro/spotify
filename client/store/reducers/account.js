const actions =  require('redux-actions')

const {
  handleActions
} = actions

module.exports = handleActions({
  'LOGOUT': (state, action) => {
    return null
  },
  'NEW_ACCOUNT': (state, action) => {
    return action.payload
  }
}, null)
