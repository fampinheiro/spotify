const reduce = require('reduce-reducers');
const redux = require('redux')

const {
  combineReducers
} = redux

module.exports =  reduce(
  combineReducers({
    account: require('./account'),
    loading: require('./noop'),
    tracks: require('./tracks'),
    raw: require('./raw')
  }),
  require('./loading')
)
