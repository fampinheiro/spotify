const array = require('force-array')
const filter = require('lodash.filter')
const actions = require('redux-actions')
const deep = require('deep-copy')
const uniq = require('lodash.uniqby')

const {
  handleActions
} = actions

module.exports = handleActions({
  'CLEAR_TRACKS': (state, action) => {
    return null
  },
  'CLOSE_TRACKS': (state, action) => {
    return filter(state, (track) => {
      return track.id !== action.payload
    })
  },
  'FETCH_TRACKS_FULFILLED': (state, action) => {
    return uniq(deep(array(action.payload, state)), 'id')
  }
}, null)
