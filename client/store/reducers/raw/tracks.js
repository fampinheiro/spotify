const actions = require('redux-actions')

const {
  handleActions
} = actions

module.exports = handleActions({
  'RAW_TRACKS': (state, action) => {
    return action.payload
  },
  'RAW_TRACKS_CLEAR': (state, action) => {
    return null
  }
}, null)
