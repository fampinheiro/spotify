const redux = require('redux')

const {
  combineReducers
} = redux

module.exports =  combineReducers({
  tracks: require('./tracks')
})
