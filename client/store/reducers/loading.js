module.exports = (state, action) => {
  if (action.type.endsWith('_PENDING')) {
    return Object.assign({}, state, {
      loading: true
    })
  }

  if (!state.loading || action.type.endsWith('_FULFILLED') || action.type.endsWith('_REJECTED')) {
    return Object.assign({}, state, {
      loading: false
    })
  }

  return state
}
