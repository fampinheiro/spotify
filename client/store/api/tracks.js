const filter = require('lodash.filter')
const get = require('lodash.get')
const map = require('lodash.map')
const fetch = require('isomorphic-fetch')
const shuffle = require('lodash.shuffle')
const uuid = require('node-uuid')

exports.search = (tracks) => {
  return fetch('/api/search', {
    body: JSON.stringify(tracks),
    method: 'post'
  }).then((response) => {
    return new Promise((resolve, reject) => {
      if (response.status === 200) {
        return resolve(response.json())
      }
      return reject(response)
    })
  }).then(transform)

  function transform (response) {
    return map(response, (item, i) => {
      if (isNumber(item)) {
        const track = tracks[i]
        return {
          id: uuid.v4(),
          title: {
            name: track.title
          },
          artist: {
            name: track.artist
          },
          message: 'Track not found on Spotify'
        }
      }

      return {
        id: get(item, 'id'),
        title: {
          name: get(item, 'name', '')
        },
        album: {
          href: get(item, 'album.external_urls.spotify', ''),
          name: get(item, 'album.name', '')
        },
        cover: get(item, 'album.images[0].url', ''),
        artist: {
          href: get(item, 'artists[0].external_urls.spotify', ''),
          name: get(item, 'artists[0].name', '')
        },
        href: get(item, 'external_urls.spotify', '')
      }
    })
  }

  function isNaN (n) {
    return Number.isNaN(Number(n))
  }

  function isNumber(n) {
    return !isNaN(n)
  }
}
