module.exports = {
  accounts: require('./accounts'),
  tracks: require('./tracks'),
  playlists: require('./playlists')
}
