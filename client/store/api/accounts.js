const camel = require('camel-case')
const keys = require('lodash.mapkeys')
const fetch = require('isomorphic-fetch')

exports.login = () => {
  return new Promise((reject, resolve) => {
    const url = window.location.toString()
    window.location = `/login?return=${encodeURIComponent(url)}`
    return resolve()
  })
}

exports.me = () => {
  return fetch('/me', {
    credentials: 'same-origin',
    method: 'get'
  }).then((response) => {
    return new Promise((resolve, reject) => {
      if (response.status === 200) {
        return resolve(response.json())
      }
      return reject(response)
    })
  }).then((response) => {
    return keys(response, (value, key) => {
      return camel(key)
    })
  })
}
