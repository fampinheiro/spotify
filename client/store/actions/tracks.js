const actions = require('redux-actions')
const api = require('../api')
const get = require('lodash.get')
const map = require('lodash.map')

const {
  createAction
} = actions


exports.clear = createAction('CLEAR_TRACKS')
exports.close = createAction('CLOSE_TRACKS')
exports.raw = createAction('RAW_TRACKS')
exports.raw.clear = createAction('RAW_TRACKS_CLEAR')
exports.raw.search = search
exports.search = createAction('FETCH_TRACKS', (tracks) => {
  return api.tracks.search(tracks)
})

function search(choices) {
  return (dispatch, getState) => {
    const state = getState()
    const columns = get(state, 'raw.tracks.columns')
    if (!columns) {
      return;
    }

    const rows = get(state, 'raw.tracks.rows')
    const tracks = map(rows, (row) => {
      return {
        'artist': row[choices.artist],
        'title': row[choices.title]
      }
    })

    return dispatch(exports.search(tracks)).then(() => {
      dispatch(exports.raw.clear())
    })
  }
}
