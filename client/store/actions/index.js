module.exports = {
  account: require('./account'),
  errors: require('./errors'),
  playlists: require('./playlists'),
  tracks: require('./tracks'),
  init: () => {
    return (dispatch) => {
      return dispatch(module.exports.account.me())
    }
  }
}
