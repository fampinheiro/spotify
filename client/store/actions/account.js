const actions = require('redux-actions')
const api = require('../api')

const {
  createAction
} = actions

exports.me = createAction('ME', () => {
  return api.accounts.me()
})

exports.login = createAction('LOGIN', () => {
  return api.accounts.login()
})

exports.logout = createAction('LOGOUT')

exports.new = createAction('NEW_ACCOUNT')
