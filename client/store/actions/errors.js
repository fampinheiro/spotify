const actions = require('redux-actions')

const {
  createAction
} = actions

exports.new = createAction('NEW_ERRORS')
