const actions = require('redux-actions')
const api = require('../api')

const {
  createAction
} = actions

exports.new = createAction('NEW_PLAYLIST', (payload) => {
  return api.playlists.new(payload)
})
