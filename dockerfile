FROM mhart/alpine-node:6.3

ENV NODE_ENV=production

WORKDIR /usr/local/app
COPY package.json package.json

RUN npm install
COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]
