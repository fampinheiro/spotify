const config = require('config')

exports.register = (server, options, next) => {
  server.auth.strategy('jwt', 'jwt', {
    key: 'never share this value',
    validateFunc: validate,
    verifyOptions: {
      algorithms: ['HS256']
    }
  });
  return next()

  function validate (decoded, request, cb) {
    return cb(null, true)
  }
}

exports.register.attributes = {
  pkg: require('./package.json')
}
