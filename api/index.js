const register = require('../register')

exports.register = (server, options, next) => {
  register(server, [
    require('hapi-auth-jwt2'),
    require('./strategies'),
    require('./login'),
    require('./search')
  ], next)
}

exports.register.attributes = {
  pkg: require('./package.json')
}
