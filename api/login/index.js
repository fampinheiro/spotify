exports.register = (server, options, next) => {
  server.route([
    require('./me-user')
  ])
  return next()
}

exports.register.attributes = {
  pkg: require('./package.json')
}
