const Boom = require('boom')
const get = require('lodash.get')
const pluck = require('lodash.pluck')
const spotify = require('../request/spotify')

module.exports = {
  path: '/me',
  method: 'get',
  config: {
    auth: 'jwt'
  },
  handler: (request, reply) => {
    const token = request.auth.credentials.token

    const options = {
      headers: {
        authorization: `Bearer ${token}`
      },
      json: true
    }

    spotify.get('/me', options, (err, res, response) => {
      if (err) {
        return reply(err)
      }

      if (res.statusCode !== 200) {
        return reply(Boom.create(res.statusCode))
      }

      return reply(response)
    })
  }
}
