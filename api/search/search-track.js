const Boom = require('boom')
const Joi = require('joi')
const get = require('lodash.get')
const pluck = require('lodash.pluck')
const remove = require('diacritics').remove;
const series = require('run-series')
const waterfall = require('run-waterfall')
const spotify = require('../request/spotify')

module.exports = {
  path: '/search',
  method: 'post',
  config: {
    validate: {
      payload: Joi.array().items({
        artist: Joi.string().required(),
        title: Joi.string().required()
      })
    }
  },
  handler: (request, reply) => {
    const payload = request.payload
    series(payload.map((item) => {
      return (cb) => {
        return query(item, cb)
      }
    }), (err, results) => {
      if (err) {
        return reply(err)
      }

      return reply(results)
    });
  }
}

const options = {
  json: true
}

function query (item, cb) {
  waterfall([
    constant,
    search,
    details
  ], cb)

  function constant (cb) {
    const {
      artist = '',
      title = ''
    } = item

    return cb(
      null,
      remove(artist).toLowerCase(),
      remove(title).toLowerCase()
    )
  }
}

function search (artist, title, cb) {
  const uri = [
    '/search?q=',
    title ? `track:"${title}" ` : '',
    artist ? `artist:"${artist}"` : '',
    '&market=pt&type=track'
  ].join('')

  spotify.get(uri, options, (err, res, response) => {
    if (err) {
      return cb(null, {
        error: err
      })
    }

    if (res.statusCode !== 200) {
      return cb(null, {
        error: res.statusCode
      })
    }

    const tracks = pluck(get(response, 'tracks.items'), 'href')
    if (tracks.length) {
      return cb(null, {
        href: tracks[0]
      })
    }

    return cb(null, {
      error: 404
    })
  })
}

function details (result, cb) {
  if (result.error || !result.href) {
    return cb(null, result.error)
  }

  spotify.get(result.href, options, (err, res, response) => {
    if (err) {
      return cb(err)
    }

    if (res.statusCode !== 200) {
      return cb(null)
    }

    return cb(null, response)
  })
}
