exports.register = (server, options, next) => {
  server.route(require('./search-track'))
  return next()
}

exports.register.attributes = {
  pkg: require('./package.json')
}
