const config = require('config')

exports.register = (server, options, next) => {
  server.register(require('bell'), (err) => {
    if (err) {
      return next(err)
    }

    server.auth.strategy('spotify', 'bell', {
      clientId: '33b5d115a6f04567a730a3a8ab33d5a4',
      clientSecret: 'b079b26e276f46cc93bb60a33e963bb3',
      isSecure: config.has('isDev') ? !config.get('isDev') : true,
      location: config.has('views.url') ? config.get('views.url') : undefined,
      password: 'this is a long password in order to avoid crack',
      provider: {
        protocol: 'oauth2',
        auth: 'https://accounts.spotify.com/authorize',
        token: 'https://accounts.spotify.com/api/token',
        scope: ['playlist-modify-private'],
        profile: (credentials, params, get, cb) => {
          return cb()
        }
      }
    })

    return next()
  })
}

exports.register.attributes = {
  pkg: require('./package.json')
}
