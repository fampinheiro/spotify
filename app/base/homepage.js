module.exports = {
  path: '/{p*}',
  method: 'get',
  handler: (request, reply) => {
    return reply.view('homepage')
  }
}
