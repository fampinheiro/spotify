const config = require('config')

exports.register = (server, options, next) => {
  server.views({
    compileOptions: {
      pretty: true
    },
    context: config.get('views'),
    engines: {
      pug: require('pug')
    },
    path: __dirname + '/views'
  });

  server.route(require('./homepage'))
  return next()
}

exports.register.attributes = {
  pkg: require('./package.json')
}
