const Boom = require('boom')
const get = require('lodash.get')
const Wreck = require('wreck')

module.exports = {
  path: '/me',
  method: 'get',
  config: {
    state: {
      parse: true
    }
  },
  handler: (request, reply) => {
    const token = get(request, 'state["spotify.user"].token')
    const options = {
      headers: {},
      json: true
    }

    if (token) {
      options.headers.authorization = token
    }

    return Wreck.get('http://localhost:3000/api/me', options, (err, res, response) => {
      if (err) {
        return reply(err)
      }

      if (res.statusCode !== 200) {
        return reply(Boom.create(res.statusCode))
      }

      return reply(response)
    })

  }
}
