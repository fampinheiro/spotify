const config = require('config')

exports.register = (server, options, next) => {
  server.state('spotify.user', {
      clearInvalid: false, // remove invalid cookies
      encoding: 'base64json',
      isSecure: config.has('isDev') ? !config.get('isDev') : true,
      isHttpOnly: true,
      strictHeader: true, // don't allow violations of RFC 6265
      ttl: null
  });

  server.route([
    require('./login-spotify'),
    require('./me-spotify')
  ])
  return next()
}

exports.register.attributes = {
  pkg: require('./package.json')
}
