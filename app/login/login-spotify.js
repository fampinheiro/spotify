const Boom = require('boom')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const get = require('lodash.get')
const pluck = require('lodash.pluck')
const Wreck = require('wreck')

module.exports = {
  path: '/login',
  method: 'get',
  config: {
    auth: 'spotify'
  },
  handler: (request, reply) => {
    const {
      return: url = '/'
    } = request.auth.credentials.query

    const state = {
      token: jwt.sign({
        refresh: request.auth.credentials.refreshToken,
        token: request.auth.credentials.token
      }, 'never share this value')
    }

    return reply().state('spotify.user', state).redirect(url)
  }
}
