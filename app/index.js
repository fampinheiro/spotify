const register = require('../register')

exports.register = (server, options, next) => {
  register(server, [
    require('vision'),
    require('./spotify'),
    require('./login'),
    require('./base')
  ], next)
}

exports.register.attributes = {
  pkg: require('./package.json')
}
