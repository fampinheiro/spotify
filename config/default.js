const path = require('path')
const pkg = require('../package.json')
const webpack = require('webpack')

const isDev = process.env.NODE_ENV === 'development'
const projectDir = path.join(__dirname, '..')
module.exports = {
  isDev: isDev,
  views: {
    url: 'http://localhost:3000'
  },
  webpack: {
    context: path.join(projectDir, 'public', 'app'),
    dev: {
      publicPath: '/public'
    },
    devtool: 'source-map',
    entry: [
      path.join(projectDir, 'client', 'app.js')
    ],
    isDev: isDev,
    module: {
      loaders: [{
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2015', 'react']
        },
        test: /\.js$/
      }, {
        loader: 'style-loader!css-loader!postcss-loader!stylus-loader',
        test: /\.styl$/
      }, {
        loader: 'json',
        test: /\.json$/
      }]
    },
    node: {
      fs: 'empty'
    },
    output: {
      filename: 'app.js',
      publicPath: '/public',
      path: path.join(projectDir, 'public')
    },
    plugins: [
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /pt/),
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new webpack.DefinePlugin({
        __DEV__: isDev
      })
    ],
    postcss: [
      require('autoprefixer')()
    ],
    stylus: {
      use: [
        require('nib')()
      ]
    }
  }
}
